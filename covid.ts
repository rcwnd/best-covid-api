import express from 'express'
import { User } from './models/user'
import { Bet } from './models/bet'
import { AddressInfo } from 'net'
import * as Crypto from 'crypto'
import * as Schedule from 'node-schedule'
import fetch from 'node-fetch'
import { Record } from './models/record'
import { GenerateSwagger } from './swagger'
import * as FS from 'fs'

/**
 * @typedef GetBet
 * @property {string} auth - user auth string
 */

/**
 * @typedef PostBet
 * @property {string} auth - user auth string
 * @property {number} value.required - How many times this user has won
 */

/**
 * @typedef PostAuth
 * @property {string} auth - user auth string
 * @property {string} token.required - new auth string
 */

/**
 * @typedef User
 * @property {string} Name.required - User ID
 * @property {string} SecretToken - Salted SHA256 hash to verify bet
 * @property {number} WinCount - How many times this user has won
 */

/**
 * @typedef Record
 * @property {string} Name.required - User ID
 * @property {number} Bet - How many people are covid people
 * @property {number} WinCount - Only source of truth
 * @property {Date} BetPlaced - When the bet was placed
 * @property {number} Diff - Delta from real covid people value
 * @property {boolean} Winner - Is this guy hero or zero
 */

const SECRET = '33d5d22defa992fa98c79e31b401b2e0063389ead05da5c7cbf7f8a33e3c828d'
const SALT = "best+in+covid+2021_"
const TEST_BUILD : boolean = false
const USER_FILE = "users.db.json"
const RESULTS_RECOVERY_FILE = ".daily_results_backup.json"
const RECORD_FILE = "history.db.json"

const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

const users: Array<User> = new Array()
const bets: Map<string, Bet> = new Map<string, Bet>()
var betsFromPreviousDay: Map<string, Bet> = new Map<string, Bet>()
var dayResults: Array<Record> = new Array()

function restoreDayResults()
{
    try 
    {
        let loadedResults = JSON.parse(FS.readFileSync(RESULTS_RECOVERY_FILE, 'utf8')) as Array<Record>
        console.log('[RECOVERY] results loaded from file cache')
        dayResults = loadedResults
    }
    catch(error)
    {
        console.log('i/o error when restoring daily results', error)
    }
}

function saveDayResults()
{
    try
    {
        FS.writeFileSync(RESULTS_RECOVERY_FILE, JSON.stringify(users))
        console.log('[RECOVERY] results saved to file cache')
    }
    catch(error)
    {
        console.log('i/o error when saving daily results', error)
    }
}

function loadUsers()
{
    try 
    {
        let loadedUsers = JSON.parse(FS.readFileSync(USER_FILE, 'utf8')) as Array<User>
        loadedUsers.forEach(user => users.push(user))
        console.log('users were loaded')
    }
    catch 
    {
        console.log('i/o error when reading users')
    }

    if (users.length == 0)
    {
        users.push(new User('sido'))
        users.push(new User('goldy'))
        users.push(new User('michal'))
        users.push(new User('mata'))
        saveUsers()
    }
}

function saveUsers()
{
    try
    {
        console.log("USERS NOW:", users)
        FS.writeFileSync(USER_FILE, JSON.stringify(users))
        console.log('users were saved')
    }
    catch
    {
        console.log('i/o error when saving users')
    }
}

function saveDailyBets()
{
    try
    {
        const currentDate = new Date().toISOString().slice(0,10)
        let fileName = '._bets_' + currentDate + ".json"
        console.log("[SAVE] Saving today bets to separate file: " + fileName)

        FS.writeFileSync(fileName, JSON.stringify(Array.from(bets)))
        console.log('[SAVE] today bets were saved')
    }
    catch
    {
        console.log('[SAVE] i/o error when saving today bets')
    }
}

function saveAllRecordsHistory()
{
    try 
    {
        let allRecords = JSON.parse(FS.readFileSync(RECORD_FILE, 'utf8')) as Array<Record>
        dayResults.forEach(result => allRecords.push(result))
        FS.writeFileSync(RECORD_FILE, JSON.stringify(allRecords))
        console.log('history was saved - total ' + allRecords.length + ' / ' + dayResults.length + ' new')
    }
    catch
    {
        try 
        {
            let allRecords: Array<Record> = []
            dayResults.forEach(result => allRecords.push(result))
            FS.writeFileSync(RECORD_FILE, JSON.stringify(allRecords))
        }
        catch
        {
            console.error('everything is wrong')
        }
    }
}

function SHA256Hash(plaintext: string): string
{
    return Crypto.createHmac('sha256', SECRET)
                   .update(SALT + plaintext)
                   .digest('hex')
}

function getUser(userId: string): User | undefined
{
    return users.find(el => el.Name === userId)
}

async function determineWinner()
{
    fetch('https://onemocneni-aktualne.mzcr.cz/api/v3/zakladni-prehled?apiToken=' + API_KEY).then(
        result => {
            result.json().then(
                data => {
                    const dataDate = data['hydra:member'][0]['datum'] as string
                    const currentDate = new Date().toISOString().slice(0,10)

                    const bigNumber = data['hydra:member'][0]['potvrzene_pripady_vcerejsi_den'] as number

                    if (dataDate !== currentDate || bigNumber === 0)
                    {
                        const pollInterval = 1000 * (50 + (Math.random() * 20))
                        setTimeout(determineWinner, pollInterval)
                        console.log("[INFO] Outdated json detected - dataDate: " + dataDate + "; dataNumber: " + bigNumber)
                        console.log('Trying to get covid data. We are too early and they are too late. Retry in ' + pollInterval)
                        return
                    }

                    console.log('today number is ', bigNumber)

                    let results = new Array<Record>()
                    let min = Number.MAX_SAFE_INTEGER
                    users.forEach(user => {
                        const userBet = bets.get(user.Name) || new Bet(user.Name, 0)

                        let diff = Math.abs(bigNumber - userBet.Value)
                        if (diff < min)
                        {
                            min = diff
                        }
                        results.push(new Record(user, userBet, diff))
                    })

                    const winDate = new Date()

                    // there could be multiple winners
                    results.filter(winners => winners.Diff <= min).forEach(winner => {
                        winner.WinCount++
                        winner.Winner = true
                        let user = getUser(winner.Name)
                        if (user)
                        {
                            console.log(winDate + ': USER ' + user.Name + ' is winner!', user, users)
                            user.WinCount++
                        }
                    })

                    // current day results
                    dayResults = results

                    // persist everything for future party and revision
                    saveDayResults()
                    saveAllRecordsHistory()
                    saveUsers()

                    // clear old bet state
                    bets.clear()

                    console.log(dataDate + ': Winners has been found. This day is done.')
                })
            .catch(
                error => {
                    console.warn('[ERROR] error fetching winner data during JSON parsing. Details: ', error, '[HTTP RESPONSE] ', result)
                    const pollInterval = 1000 * 5 * 60 // 5 mins
                    setTimeout(determineWinner, pollInterval)
                    console.log('Trying to get covid data. Error when parsing JSON. Retry in ' + pollInterval)
                }
            )
        }
    ).catch(
        error => {
            console.warn('[ERROR] error fetching winner data during FETCH. Details: ', error)
            const pollInterval = 1000 * 5 * 60 // 5 mins
            setTimeout(determineWinner, pollInterval)
            console.log('Trying to get covid data. Error when fetching http result. Retry in ' + pollInterval)
        }
    )
}

/**
 * Api version
 * @route GET /
 * @group api - general endpoints
 * @returns {string} 200 - API description
 */
 
app.get('/', (req, res) => {
    res.send("best-covid-api-2.0-2022-01-02 [ο]")
})

/**
 * Get all users
 * @route GET /users
 * @group users - user related endpoints
 * @returns {Array<User>} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
app.get('/users', (req, res) => {
    res.send(users)
})

/**
 * Get specific user
 * @route GET /users/{id}
 * @group users - user related endpoints
 * @param {string} id.path.required - username
 * @returns {User} 200 - User
 * @returns {Error}  404 - User not found
 */
app.get('/users/:id', (req, res) => {
    const userId: string = req.params.id as string
    const user = getUser(userId)

    if (user)
    {
        res.send(user)
    }
    else
    {
        res.sendStatus(404)
    }
})

/**
 * Change user auth token
 * @route POST /users/{id}
 * @group users - user related endpoints
 * @param {string} id.path.required - username 
 * @param {PostAuth.model} value.body.required - auth data
 * @returns {string} 200 - token changed
 * @returns {Error}  404 - User not found
 * @returns {Error}  400 - Bad token
 */
app.post('/users/:id', (req, res) => {
    const userId: string = req.params.id as string
    const token: string = req.body.token as string
    const auth: string = req.body.auth as string
    const user = getUser(userId)
    
    if (user)
    {
        if (user.SecretToken === "" || user.SecretToken === SHA256Hash(auth))
        {
            user.SecretToken = SHA256Hash(token)
            saveUsers()
            res.sendStatus(200)
        }
        else
        {
            res.sendStatus(400) // bad hash
        }
    }
    else
    {
        res.sendStatus(404) // user not found
    }
})

/**
 * Get all bets from previous day. Updates every day just moments before the first contact to MZCR server.
 * @route GET /bet
 * @group bets - bet related endpoints
 * @returns {Bet} 200 - bets from previous day
 */
 app.get('/bet', (req, res) => {
    res.status(200).send(Array.from(betsFromPreviousDay))
})

/**
 * Get current bet for specific user 
 * @route PATCH /bet/{id}
 * @group bets - bet related endpoints
 * @param {string} id.path.required - username 
 * @param {GetBet.model} value.body.required - auth data
 * @returns {Bet} 200 - user's current bet
 * @returns {Error}  404 - User not found
 * @returns {Error}  400 - Bad token
 */
 app.patch('/bet/:id', (req, res) => {
    const userId: string = req.params.id as string
    const auth: string = req.body.auth as string
    const user = getUser(userId)
    
    if (user)
    {
        if (user.SecretToken === "" || user.SecretToken === SHA256Hash(auth))
        {
            const bet = bets.get(user.Name) || new Bet(user.Name, 0)
            res.status(200).send(bet)
        }
        else
        {
            res.sendStatus(400) // bad hash
        }
    }
    else
    {
        res.sendStatus(404) // user not found
    }
})


/**
 * Place bet
 * @route POST /bet/{id}
 * @group bets - bet related endpoints
 * @param {string} id.path.required - username 
 * @param {PostBet.model} value.body.required - new bet
 * @returns {string} 200 - bet accepted
 * @returns {Error}  404 - User not found
 * @returns {Error}  400 - Bad token
 * @returns {Error}  451 - Bets not allowed from 03am to 10am
 */
 app.post('/bet/:id', (req, res) => {
    const userId: string = req.params.id as string
    const value: number = req.body.value as number
    const auth: string = req.body.auth as string
    const user = getUser(userId)

    const currentDate = new Date()
    if ((currentDate.getHours() >= 3) &&
        (currentDate.getHours() <= 9))
    {
        res.sendStatus(451)
        return
    }
    
    if (user)
    {
        if (user.SecretToken === "" || user.SecretToken === SHA256Hash(auth))
        {
            const bet = new Bet(user.Name, value)
            bets.set(user.Name, bet)
            res.sendStatus(200)
        }
        else
        {
            res.sendStatus(400) // bad hash
        }
    }
    else
    {
        res.sendStatus(404) // user not found
    }
})

/**
 * Get winner users
 * @route GET /winners
 * @group winners - results endpoints
 * @returns {Array<Record>} 200 - Result list
 */
 app.get('/winners', (req, res) => {
    res.send(dayResults)
})

/**
 * Test winner users
 * @route GET /test/winners
 * @group test - test endpoints
 * @returns {string} 200 - Result list
 */
 app.get('/test/winners', (req, res) => {
     if (TEST_BUILD)
     {
         determineWinner()
         res.sendStatus(200)
     }
     else
     {
         res.status(400).send("This is production build.")
     }
})

Schedule.scheduleJob('00 00 08 * * *', () => {
    saveDailyBets()
    betsFromPreviousDay.clear()
    bets.forEach(bet => {
        betsFromPreviousDay.set(bet.User, Bet.copy(bet))
    })
}) 

Schedule.scheduleJob('00 02 08 * * *', () => {
    determineWinner()
}) 

GenerateSwagger(app)

loadUsers()
restoreDayResults()

var API_KEY = process.env.COVID_KEY
if (API_KEY === "") 
{
    console.error(">>>> COVID API key was not found !!!! PASS it using COVID_KEY enviroment variable. <<<<<")
}

const server = app.listen(5000, '0.0.0.0', () => {
    const {port, address} = server.address() as AddressInfo
    console.log('Covid Server listening on:','http://' + address + ':'+port)
})

export {app}
