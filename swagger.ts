
export function GenerateSwagger(app: any) {
    const expressSwagger = require('express-swagger-generator')(app);

    let options = {
        swaggerDefinition: {
            info: {
                description: 'Best Covid API Omicron',
                title: 'Swagger',
                version: '2.0.1',
            },
            host: 'forpres.cz:5000',
            // host: '127.0.0.1:5000',
            basePath: '/',
            produces: [
                "application/json",
            ],
            schemes: ['http', 'https'],
            securityDefinitions: {
                JWT: {
                    type: 'apiKey',
                    in: 'header',
                    name: 'Authorization',
                    description: "",
                }
            }
        },
        basedir: __dirname, //app absolute path
        files: ['./*.js'] //Path to the API handle folder
    };
    let x = expressSwagger(options)
    console.log (x)
}
