
export class Bet {
    public Value: number = 0
    public BetDate: Date = new Date()
    public User: string
    
    constructor(user: string, value: number)
    {
        this.User = user
        this.Value = value
    }

    static copy(original: Bet) 
    {
        let result = new Bet(original.User, original.Value)
        result.BetDate = new Date(original.BetDate)
        return result
    }
}