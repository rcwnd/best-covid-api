import { Bet } from "./bet"
import { User } from "./user"

export class Record {
    public Name: string
    public Bet: number
    public BetPlaced: Date
    public WinCount: number
    public Diff: number
    public Winner: boolean

    constructor(user: User, bet: Bet, diff: number, winner: boolean = false)
    {
        this.Name = user.Name
        this.Bet = bet.Value
        this.BetPlaced = bet.BetDate
        this.WinCount = user.WinCount
        this.Diff = diff
        this.Winner = winner
    }
}