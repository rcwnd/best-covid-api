import { Bet } from "./bet"

export class User {
    public Name: string = ""
    public SecretToken: string = ""
    public WinCount: number = 0

    constructor (name: string)
    {
        this.Name = name
    }
}